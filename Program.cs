﻿using System;
using System.Text;
using System.Threading.Tasks;

namespace operacionfibonacci
{
    class Program
    {
        static void Main(string[] args)
        {
            //Sucesión de Fibonacci
            int n, primero = 0, segundo = 1, siguiente;
            Console.Write("Digite el valor de n: ");
            n = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i <= n; i++)
            {
                if (i <= 1)
                {
                    siguiente = i;
                }
                else
                {
                    siguiente = primero + segundo;
                    primero = segundo;
                    segundo = siguiente;
                }
                Console.WriteLine("{0} - ", siguiente);
            }
            Console.ReadKey();
        }
    }
}